<?php

include_once("./controllers/controller.php");

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sàn Vật Chất</title>
    <link rel="stylesheet" href="Libs/css/bootstrap.css ">
    <link rel="stylesheet" href="Libs/css/style-home.css ">
    <link rel="stylesheet" href="Libs/owlcarousel/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="Libs/owlcarousel/assets/owl.theme.default.min.css">
    <link rel="stylesheet" href="Libs/fontawesome-free-5.15.3-web/css/all.min.css" />
    <link rel="stylesheet" href="Libs/font-awesome-4.7.0/css/font-awesome.min.css" />
    <script src="Libs/js/jquery-3.5.1.min.js"></script>
    <script src="Libs/js/bootstrap.js"></script>
    <script src="Libs/owlcarousel/owl.carousel.min.js"></script>

</head>

<body>


<!-- header -->
<div id="header" class="relative">
    <div class="topheader ">
        <div class="content">
            <a class="hotline " style="color:#ffc500;margin-right:50px;">Hotline:1900.1234-091234567</a>
            <a class="sign " style="color:#b8babd;" href="#">
                <i class="fas fa-user" style="color:#b8babd;margin:5px;"></i>Đăng ký</a>
            <a class="sign" style="color:#b8babd;">/</a>
            <a class="sign" style="color:#b8babd;" href="#">Đăng nhập</a>
        </div>
    </div>
    <div class="midheader">
        <div class="container botheader">
            <div class="row">
                <div class="col-lg-2">
                    <a href="#"><img class="img-fluid" src="Libs/img-sanvchat01/logo.png"></a>
                </div>
                <div id="search-text" class="col-lg-8">
                    <form class=" form-inline">
                        <input class="form-control" type="search" name="Tìm kiếm" placeholder="Bạn tìm kiếm gì ?"
                               aria-label="Search">
                        <button class="btn my-2 my-sm-0" type="submit"><i class="fas fa-search"></i></button>
                    </form>
                </div>
                <div id="cart-notify" class="col-lg-2">
                    <button class="btn" type="submit">Đăng bán miễn phí</button>
                    <a><i class="fas fa-shopping-cart fa-lg"></i></a>
                </div>
            </div>

        </div>
    </div>
    <div class="endheader">
        <div>
            <ul>
                <li class="dropdown">
                    <a href="#" class="dropbtn">Danh mục sản phẩm<span style="margin-left:15px;"><i
                                    class="fas fa-caret-down"></i></span></a>
                    <ul class="dropdown-content">
                        <li id="level1" class="cat1">
                            <a href="#" class="cat-level1 "><span
                                ><i class="fa fa-angle-right" aria-hidden="true"></i></span>Thiết bị điện tử
                            </a>
                            <ul id="cat1-level2" class="cat-level2">
                                <?php
                                for ($i = 0; $i <= $row_num - 1; $i++) {
                                    if ($row[$i]['cat_parent'] == 1) {
                                        $cat_name = $row[$i]['cat_name'];
                                        echo '<li class="cat1-level2-no' . $row[$i]["id_level2"] . '"><a href="#">' . $cat_name . '</a>';
                                        $id_level2 = $row[$i]['cat_id']; ?>
                                        <ul class="cat-level3" id="cat<?php echo $row[$i]['id_level2']; ?>-level3">
                                            <?php for ($j = 0; $j <= $row_num - 1; $j++) {
                                                if ($row[$j]['cat_parent'] == $id_level2) { ?>
                                                    <span><a href="#"><?php echo $row[$j]['cat_name']; ?></a></span>
                                                <?php }
                                            } ?>
                                        </ul></li>
                                    <?php }
                                }
                                ?>
                            </ul>
                        </li>
                        <li id="level2" class="cat1">
                            <a href="#" class="cat-level1"><span
                                ><i class="fa fa-angle-right" aria-hidden="true"></i></span>Hàng mẹ và bé
                            </a>
                            <ul id="cat2-level2" class="cat-level2">
                                <?php
                                for ($i = 0; $i <= $row_num - 1; $i++) {
                                    if ($row[$i]['cat_parent'] == 2) {
                                        $cat_name = $row[$i]['cat_name'];
                                        echo '<li class="cat2-level2-no' . $row[$i]["id_level2"] . '"><a href="#">' . $cat_name . '</a>';
                                        $id_level2 = $row[$i]['cat_id']; ?>
                                        <ul class="cat-level3" id="cat<?php echo $row[$i]['id_level2']; ?>-level3">
                                            <?php for ($j = 0; $j <= $row_num - 1; $j++) {
                                                if ($row[$j]['cat_parent'] == $id_level2) { ?>

                                                    <span><a href="#"><?php echo $row[$j]['cat_name']; ?></a></span>

                                                <?php }
                                            } ?>
                                        </ul></li>
                                    <?php }
                                }
                                ?>

                            </ul>
                        </li>
                        <li id="level3" class="cat1">
                            <a href="#" class="cat-level1" id="catno3"><span
                                ><i class="fa fa-angle-right" aria-hidden="true"></i></span>Phụ kiện điện tử
                            </a>
                            <ul id="cat3-level2" class="cat-level2">
                                <?php
                                for ($i = 0; $i <= $row_num - 1; $i++) {
                                    if ($row[$i]['cat_parent'] == 2) {
                                        $cat_name = $row[$i]['cat_name'];
                                        echo '<li class="cat1-level2-no' . $row[$i]["id_level2"] . '"><a href="#">' . $cat_name . '</a>';
                                        $id_level2 = $row[$i]['cat_id']; ?>
                                        <ul class="cat-level3" id="cat<?php echo $row[$i]['id_level2']; ?>-level3">
                                            <?php for ($j = 0; $j <= $row_num - 1; $j++) {
                                                if ($row[$j]['cat_parent'] == $id_level2) { ?>

                                                    <span><a href="#"><?php echo $row[$j]['cat_name']; ?></a></span>

                                                <?php }
                                            } ?>
                                        </ul></li>
                                    <?php }
                                }
                                ?>
                            </ul>
                        </li>
                        <li id="level4" class="cat1">
                            <a href="#" class="cat-level1" id="catno4"><span
                                ><i class="fa fa-angle-right" aria-hidden="true"></i></span>Thiết bị điện tử
                            </a>
                            <ul id="cat4-level2" class="cat-level2">
                                <?php
                                for ($i = 0; $i <= $row_num - 1; $i++) {
                                    if ($row[$i]['cat_parent'] == 2) {
                                        $cat_name = $row[$i]['cat_name'];
                                        echo '<li class="cat1-level2-no' . $row[$i]["id_level2"] . '"><a href="#">' . $cat_name . '</a>';
                                        $id_level2 = $row[$i]['cat_id']; ?>
                                        <ul class="cat-level3" id="cat<?php echo $row[$i]['id_level2']; ?>-level3">
                                            <?php for ($j = 0; $j <= $row_num - 1; $j++) {
                                                if ($row[$j]['cat_parent'] == $id_level2) { ?>

                                                    <span><a href="#"><?php echo $row[$j]['cat_name']; ?></a></span>

                                                <?php }
                                            } ?>
                                        </ul></li>
                                    <?php }
                                }
                                ?>

                            </ul>
                        </li>
                        <li id="level5" class="cat1">
                            <a href="#" class="cat-level1" id="catno5"><span
                                ><i class="fa fa-angle-right" aria-hidden="true"></i></span>Phụ kiện điện tử
                            </a>
                            <ul id="cat5-level2" class="cat-level2">
                                <?php
                                for ($i = 0; $i <= $row_num - 1; $i++) {
                                    if ($row[$i]['cat_parent'] == 2) {
                                        $cat_name = $row[$i]['cat_name'];
                                        echo '<li class="cat1-level2-no' . $row[$i]["id_level2"] . '"><a href="#">' . $cat_name . '</a>';
                                        $id_level2 = $row[$i]['cat_id']; ?>
                                        <ul class="cat-level3" id="cat<?php echo $row[$i]['id_level2']; ?>-level3">
                                            <?php for ($j = 0; $j <= $row_num - 1; $j++) {
                                                if ($row[$j]['cat_parent'] == $id_level2) { ?>

                                                    <span><a href="#"><?php echo $row[$j]['cat_name']; ?></a></span>

                                                <?php }
                                            } ?>
                                        </ul></li>
                                    <?php }
                                }
                                ?>

                            </ul>
                        </li>
                        <li id="level6" class="cat1">
                            <a href="#" class="cat-level1" id="catno6"><span
                                ><i class="fa fa-angle-right" aria-hidden="true"></i></span>Thiết bị gia dụng
                            </a>
                            <ul id="cat6-level2" class="cat-level2">
                                <?php
                                for ($i = 0; $i <= $row_num - 1; $i++) {
                                    if ($row[$i]['cat_parent'] == 2) {
                                        $cat_name = $row[$i]['cat_name'];
                                        echo '<li class="cat1-level2-no' . $row[$i]["id_level2"] . '"><a href="#">' . $cat_name . '</a>';
                                        $id_level2 = $row[$i]['cat_id']; ?>
                                        <ul class="cat-level3" id="cat<?php echo $row[$i]['id_level2']; ?>-level3">
                                            <?php for ($j = 0; $j <= $row_num - 1; $j++) {
                                                if ($row[$j]['cat_parent'] == $id_level2) { ?>

                                                    <span><a href="#"><?php echo $row[$j]['cat_name']; ?></a></span>

                                                <?php }
                                            } ?>
                                        </ul></li>
                                    <?php }
                                }
                                ?>

                            </ul>
                        </li>
                        <li id="level7" class="cat1">
                            <a href="#" class="cat-level1" id="catno7"><span
                                ><i class="fa fa-angle-right" aria-hidden="true"></i></span>Sức khỏe,làm đẹp
                            </a>
                            <ul id="cat7-level2" class="cat-level2">
                                <?php
                                for ($i = 0; $i <= $row_num - 1; $i++) {
                                    if ($row[$i]['cat_parent'] == 2) {
                                        $cat_name = $row[$i]['cat_name'];
                                        echo '<li class="cat1-level2-no' . $row[$i]["id_level2"] . '"><a href="#">' . $cat_name . '</a>';
                                        $id_level2 = $row[$i]['cat_id']; ?>
                                        <ul class="cat-level3" id="cat<?php echo $row[$i]['id_level2']; ?>-level3">
                                            <?php for ($j = 0; $j <= $row_num - 1; $j++) {
                                                if ($row[$j]['cat_parent'] == $id_level2) { ?>

                                                    <span><a href="#"><?php echo $row[$j]['cat_name']; ?></a></span>

                                                <?php }
                                            } ?>
                                        </ul></li>
                                    <?php }
                                }
                                ?>

                            </ul>
                        </li>
                        <li id="level8" class="cat1">
                            <a href="#" class="cat-level1" id="catno8"><span
                                ><i class="fa fa-angle-right" aria-hidden="true"></i></span>Hàng mẹ,bé
                            </a>
                            <ul id="cat8-level2" class="cat-level2">
                                <?php
                                for ($i = 0; $i <= $row_num - 1; $i++) {
                                    if ($row[$i]['cat_parent'] == 2) {
                                        $cat_name = $row[$i]['cat_name'];
                                        echo '<li class="cat1-level2-no' . $row[$i]["id_level2"] . '"><a href="#">' . $cat_name . '</a>';
                                        $id_level2 = $row[$i]['cat_id']; ?>
                                        <ul class="cat-level3" id="cat<?php echo $row[$i]['id_level2']; ?>-level3">
                                            <?php for ($j = 0; $j <= $row_num - 1; $j++) {
                                                if ($row[$j]['cat_parent'] == $id_level2) { ?>

                                                    <span><a href="#"><?php echo $row[$j]['cat_name']; ?></a></span>

                                                <?php }
                                            } ?>
                                        </ul></li>
                                    <?php }
                                }
                                ?>

                            </ul>
                        </li>
                        <li id="level9" class="cat1">
                            <a href="#" class="cat-level1" id="catno9"><span
                                ><i class="fa fa-angle-right" aria-hidden="true"></i></span>Siêu thị ,tạp hóa
                            </a>
                            <ul id="cat9-level2" class="cat-level2">
                                <?php
                                for ($i = 0; $i <= $row_num - 1; $i++) {
                                    if ($row[$i]['cat_parent'] == 2) {
                                        $cat_name = $row[$i]['cat_name'];
                                        echo '<li class="cat1-level2-no' . $row[$i]["id_level2"] . '"><a href="#">' . $cat_name . '</a>';
                                        $id_level2 = $row[$i]['cat_id']; ?>
                                        <ul class="cat-level3" id="cat<?php echo $row[$i]['id_level2']; ?>-level3">
                                            <?php for ($j = 0; $j <= $row_num - 1; $j++) {
                                                if ($row[$j]['cat_parent'] == $id_level2) { ?>

                                                    <span><a href="#"><?php echo $row[$j]['cat_name']; ?></a></span>

                                                <?php }
                                            } ?>
                                        </ul></li>
                                    <?php }
                                }
                                ?>

                            </ul>
                        </li>
                        <li id="level10" class="cat1">
                            <a href="#" class="cat-level1" id="catno10"><span
                                ><i class="fa fa-angle-right" aria-hidden="true"></i></span>Gia dụng
                            </a>
                            <ul id="cat10-level2" class="cat-level2">
                                <?php
                                for ($i = 0; $i <= $row_num - 1; $i++) {
                                    if ($row[$i]['cat_parent'] == 2) {
                                        $cat_name = $row[$i]['cat_name'];
                                        echo '<li class="cat1-level2-no' . $row[$i]["id_level2"] . '"><a href="#">' . $cat_name . '</a>';
                                        $id_level2 = $row[$i]['cat_id']; ?>
                                        <ul class="cat-level3" id="cat<?php echo $row[$i]['id_level2']; ?>-level3">
                                            <?php for ($j = 0; $j <= $row_num - 1; $j++) {
                                                if ($row[$j]['cat_parent'] == $id_level2) { ?>

                                                    <span><a href="#"><?php echo $row[$j]['cat_name']; ?></a></span>

                                                <?php }
                                            } ?>
                                        </ul></li>
                                    <?php }
                                }
                                ?>

                            </ul>
                        </li>
                        <li id="level11" class="cat1">
                            <a href="#" class="cat-level1" id="catno11"><span
                                ><i class="fa fa-angle-right" aria-hidden="true"></i></span>Thời trang nữ
                            </a>
                            <ul id="cat11-level2" class="cat-level2">
                                <?php
                                for ($i = 0; $i <= $row_num - 1; $i++) {
                                    if ($row[$i]['cat_parent'] == 2) {
                                        $cat_name = $row[$i]['cat_name'];
                                        echo '<li class="cat1-level2-no' . $row[$i]["id_level2"] . '"><a href="#">' . $cat_name . '</a>';
                                        $id_level2 = $row[$i]['cat_id']; ?>
                                        <ul class="cat-level3" id="cat<?php echo $row[$i]['id_level2']; ?>-level3">
                                            <?php for ($j = 0; $j <= $row_num - 1; $j++) {
                                                if ($row[$j]['cat_parent'] == $id_level2) { ?>

                                                    <span><a href="#"><?php echo $row[$j]['cat_name']; ?></a></span>

                                                <?php }
                                            } ?>
                                        </ul></li>
                                    <?php }
                                }
                                ?>

                            </ul>
                        </li>
                        <li id="level12" class="cat1">
                            <a href="#" class="cat-level1" id="catno12"><span
                                ><i class="fa fa-angle-right" aria-hidden="true"></i></span>Thời trang nam
                            </a>
                            <ul id="cat12-level2" class="cat-level2">
                                <?php
                                for ($i = 0; $i <= $row_num - 1; $i++) {
                                    if ($row[$i]['cat_parent'] == 2) {
                                        $cat_name = $row[$i]['cat_name'];
                                        echo '<li class="cat1-level2-no' . $row[$i]["id_level2"] . '"><a href="#">' . $cat_name . '</a>';
                                        $id_level2 = $row[$i]['cat_id']; ?>
                                        <ul class="cat-level3" id="cat<?php echo $row[$i]['id_level2']; ?>-level3">
                                            <?php for ($j = 0; $j <= $row_num - 1; $j++) {
                                                if ($row[$j]['cat_parent'] == $id_level2) { ?>

                                                    <span><a href="#"><?php echo $row[$j]['cat_name']; ?></a></span>

                                                <?php }
                                            } ?>
                                        </ul></li>
                                    <?php }
                                }
                                ?>

                            </ul>
                        </li>
                    </ul>
                </li>
                <li><a href="#"><span style="margin-right:6px;"><i class="fa fa-volume-up"
                                                                   aria-hidden="true"></i></span>Tin tức </a></li>
                <li><a href="#"><span style="margin-right:6px;"><i class="fas fa-users"></i></span>Cộng đồng</a>
                </li>
                <li><a href="#"><span style="margin-right:6px;"><i class="fas fa-download"></i></span>Tải app</a>
                </li>
            </ul>
        </div>
    </div>
    <!-- extrabar -->
    <div class="absolute rightbar">
        <ul class="list">
            <li href="#"><i class="fas fa-shopping-cart fa-2x" style="color:#fec505;margin-left:-32px;"></i>
                <p> Giỏ Hàng</p>
            </li>
            <li href="#"><i class="fas fa-user-alt  fa-2x" style="color:#fff;margin-left:-32px;"></i>
                <p>Tài Khoản</p>
            </li>
            <li href="#"><i class="fas fa-heart  fa-2x" style="color:#fff;margin-left:-32px;"></i>
                <p>Yêu Thích</p>
            </li>
            <li href="#"><i class="fas fa-redo fa-2x" style="color:#fff;margin-left:-32px;"></i>
                <p>Đã Xem</p>
            </li>
        </ul>
    </div>
    <div class="absolute leftbar">
        <div
                style="width:36px; height:36px;background-color: #0b67c6;padding:6px 10px;margin:0 0 3px -15px;color:#fff">
            <i class="fas fa-mobile-alt fa-2x"></i>
        </div>
        <div
                style="width:36px; height:36px;background-color: #ccc;padding:6px 11px;margin:0 0 3px -15px;color:#fff;">
            <i class="fa fa-female fa-2x" aria-hidden="true"></i>
        </div>
        <div
                style="width:36px; height:36px;background-color: #ccc;padding:6px 4px;margin:0 0 3px -15px;color:#fff;">
            <i class="fa fa-laptop fa-2x" aria-hidden="true"></i>
        </div>
        <div
                style="width:36px; height:36px;background-color: #ccc;padding:6px 10px;margin:0 0 3px -15px;color:#fff;">
            <i class="fas fa-mobile-alt fa-2x"></i>
        </div>
        <div
                style="width:36px; height:36px;background-color: #ccc;padding:6px 11px;margin:0 0 3px -15px;color:#fff;">
            <i class="fa fa-female fa-2x" aria-hidden="true"></i>
        </div>
        <div
                style="width:36px; height:36px;background-color: #ccc;padding:6px 4px;margin:0 0 3px -15px;color:#fff;">
            <i class="fa fa-laptop fa-2x" aria-hidden="true"></i>
        </div>
        <div
                style="width:36px; height:36px;background-color: #ccc;padding:6px 10px;margin:0 0 3px -15px;color:#fff;">
            <i class="fas fa-mobile-alt fa-2x"></i>
        </div>
        <div
                style="width:36px; height:36px;background-color: #ccc;padding:6px 11px;margin:0 0 3px -15px;color:#fff;">
            <i class="fa fa-female fa-2x" aria-hidden="true"></i>
        </div>
        <div
                style="width:36px; height:36px;background-color: #ccc;padding:6px 4px;margin:0 0 3px -15px;color:#fff;">
            <i class="fa fa-laptop fa-2x" aria-hidden="true"></i>
        </div>
        <div
                style="width:36px; height:36px;background-color: #ccc;padding:6px 10px;margin:0 0 3px -15px;color:#fff;">
            <i class="fas fa-mobile-alt fa-2x"></i>
        </div>
        <div
                style="width:36px; height:36px;background-color: #ccc;padding:6px 11px;margin:0 0 3px -15px;color:#fff;">
            <i class="fa fa-female fa-2x" aria-hidden="true"></i>
        </div>
        <div
                style="width:36px; height:36px;background-color: #ccc;padding:6px 4px;margin:0 0 3px -15px;color:#fff;">
            <i class="fa fa-laptop fa-2x" aria-hidden="true"></i>
        </div>
        <div
                style="width:36px; height:36px;background-color: #ccc;padding:6px 10px;margin:0 0 3px -15px;color:#fff;">
            <i class="fas fa-mobile-alt fa-2x"></i>
        </div>
    </div>
</div>


<!-- mainads -->
<div id="mainads">
    <div style="height:375px;width:906px; background-color: #fff;margin-left: 374px;">
        <img class="img-fluid xakho" src="Libs/img-sanvchat01/xakho.png">
        <div class="rightimage">
            <img class="img-fluid " src="Libs/img-sanvchat01/salehot.png">
            <img class="img-fluid mt-1 " src="Libs/img-sanvchat01/tailocmoi.png">
        </div>
    </div>
</div>

<!-- main -->
<div id="main" style="background-color: #f7f7f7;">

    <div id="slide" class="container">
        <h3>Xu hướng tìm kiếm</h3>
        <div class="row" style=" width: 1175px;position:relative;">

            <div class="owl-carousel owl-theme">
                <div class="item">
                    <img class="img-fluid" src="Libs/img-sanvchat01/phone.png">
                    <a href="#">
                        <p>Điện thoại iphone X dung lượng 256gb</p>
                    </a>
                    <p>Từ <span>9.950.000đ</span></p>
                    <p>có 4 nơi bán </p>
                    <p> (10)
                        <i class="fas fa-star" style="color:#879596"></i>
                        <i class="fas fa-star" style="color:#879596"></i>
                        <i class="fas fa-star" style="color:#879596"></i>
                        <i class="fas fa-star" style="color:#879596"></i>
                        <i class="fas fa-star" style="color:#879596"></i>
                    </p>
                </div>
                <div class="item">
                    <img class="img-fluid" src="Libs/img-sanvchat01/phone.png">
                    <a href="#">
                        <p>Điện thoại iphone X dung lượng 256gb</p>
                    </a>
                    <p>Từ <span>9.950.000đ</span></p>
                    <p>có 4 nơi bán </p>
                    <p> (10)
                        <i class="fas fa-star" style="color:#879596"></i>
                        <i class="fas fa-star" style="color:#879596"></i>
                        <i class="fas fa-star" style="color:#879596"></i>
                        <i class="fas fa-star" style="color:#879596"></i>
                        <i class="fas fa-star" style="color:#879596"></i>
                    </p>
                </div>
                <div class="item">
                    <img class="img-fluid" src="Libs/img-sanvchat01/phone.png">
                    <a href="#">
                        <p>Điện thoại iphone X dung lượng 256gb</p>
                    </a>
                    <p>Từ <span>9.950.000đ</span></p>
                    <p>có 4 nơi bán </p>
                    <p> (10)
                        <i class="fas fa-star" style="color:#879596"></i>
                        <i class="fas fa-star" style="color:#879596"></i>
                        <i class="fas fa-star" style="color:#879596"></i>
                        <i class="fas fa-star" style="color:#879596"></i>
                        <i class="fas fa-star" style="color:#879596"></i>
                    </p>
                </div>
                <div class="item">
                    <img class="img-fluid" src="Libs/img-sanvchat01/phone.png">
                    <a href="#">
                        <p>Điện thoại iphone X dung lượng 256gb</p>
                    </a>
                    <p>Từ <span>9.950.000đ</span></p>
                    <p>có 4 nơi bán </p>
                    <p> (10)
                        <i class="fas fa-star" style="color:#879596"></i>
                        <i class="fas fa-star" style="color:#879596"></i>
                        <i class="fas fa-star" style="color:#879596"></i>
                        <i class="fas fa-star" style="color:#879596"></i>
                        <i class="fas fa-star" style="color:#879596"></i>
                    </p>
                </div>
                <div class="item">
                    <img class="img-fluid" src="Libs/img-sanvchat01/phone.png">
                    <a href="#">
                        <p>Điện thoại iphone X dung lượng 256gb</p>
                    </a>
                    <p>Từ <span>9.950.000đ</span></p>
                    <p>có 4 nơi bán </p>
                    <p> (10)
                        <i class="fas fa-star" style="color:#879596"></i>
                        <i class="fas fa-star" style="color:#879596"></i>
                        <i class="fas fa-star" style="color:#879596"></i>
                        <i class="fas fa-star" style="color:#879596"></i>
                        <i class="fas fa-star" style="color:#879596"></i>
                    </p>
                </div>
                <div class="item">
                    <img class="img-fluid" src="Libs/img-sanvchat01/phone.png">
                    <a href="#">
                        <p>Điện thoại iphone X dung lượng 256gb</p>
                    </a>
                    <p>Từ <span>9.950.000đ</span></p>
                    <p>có 4 nơi bán </p>
                    <p> (10)
                        <i class="fas fa-star" style="color:#879596"></i>
                        <i class="fas fa-star" style="color:#879596"></i>
                        <i class="fas fa-star" style="color:#879596"></i>
                        <i class="fas fa-star" style="color:#879596"></i>
                        <i class="fas fa-star" style="color:#879596"></i>
                    </p>
                </div>
                <div class="item">
                    <img class="img-fluid" src="Libs/img-sanvchat01/phone.png">
                    <a href="#">
                        <p>Điện thoại iphone X dung lượng 256gb</p>
                    </a>
                    <p>Từ <span>9.950.000đ</span></p>
                    <p>có 4 nơi bán </p>
                    <p> (10)
                        <i class="fas fa-star" style="color:#879596"></i>
                        <i class="fas fa-star" style="color:#879596"></i>
                        <i class="fas fa-star" style="color:#879596"></i>
                        <i class="fas fa-star" style="color:#879596"></i>
                        <i class="fas fa-star" style="color:#879596"></i>
                    </p>
                </div>
                <div class="item">
                    <img class="img-fluid" src="Libs/img-sanvchat01/phone.png">
                    <a href="#">
                        <p>Điện thoại iphone X dung lượng 256gb</p>
                    </a>
                    <p>Từ <span>9.950.000đ</span></p>
                    <p>có 4 nơi bán </p>
                    <p> (10)
                        <i class="fas fa-star" style="color:#879596"></i>
                        <i class="fas fa-star" style="color:#879596"></i>
                        <i class="fas fa-star" style="color:#879596"></i>
                        <i class="fas fa-star" style="color:#879596"></i>
                        <i class="fas fa-star" style="color:#879596"></i>
                    </p>
                </div>
                <div class="item">
                    <img class="img-fluid" src="Libs/img-sanvchat01/phone.png">
                    <a href="#">
                        <p>Điện thoại iphone X dung lượng 256gb</p>
                    </a>
                    <p>Từ <span>9.950.000đ</span></p>
                    <p>có 4 nơi bán </p>
                    <p> (10)
                        <i class="fas fa-star" style="color:#879596"></i>
                        <i class="fas fa-star" style="color:#879596"></i>
                        <i class="fas fa-star" style="color:#879596"></i>
                        <i class="fas fa-star" style="color:#879596"></i>
                        <i class="fas fa-star" style="color:#879596"></i>
                    </p>
                </div>
                <div class="item">
                    <img class="img-fluid" src="Libs/img-sanvchat01/phone.png">
                    <a href="#">
                        <p>Điện thoại iphone X dung lượng 256gb</p>
                    </a>
                    <p>Từ <span>9.950.000đ</span></p>
                    <p>có 4 nơi bán </p>
                    <p> (10)
                        <i class="fas fa-star" style="color:#879596"></i>
                        <i class="fas fa-star" style="color:#879596"></i>
                        <i class="fas fa-star" style="color:#879596"></i>
                        <i class="fas fa-star" style="color:#879596"></i>
                        <i class="fas fa-star" style="color:#879596"></i>
                    </p>
                </div>


            </div>
            <!-- <div
                style="position:absolute;z-index:3;top:60px;left:0px;box-shadow:10px 4px 7px   #c8c8c8 ;padding:33px 7px 33px 18px;border-radius: 0 2px 2px 0;">
                <i class="fa fa-chevron-left fa-2x" aria-hidden="true"></i></div>
            <div style="position:absolute;z-index: 3;top:80px;right:16px;"><i class="fa fa-chevron-right fa-2x"
                    aria-hidden="true"></i></div> -->

        </div>
    </div>

    <div id="maincontent">

        <div class="container content1" style="position:relative;">
            <div
                    style="width:26px; height:26px;background-color: #0b67c6;padding:1px 8px;margin:0 0 20px -15px;color: #fff;">
                <i class="fas fa-mobile-alt"></i>
            </div>
            <h4 class="title-maincontent" style="position:absolute;top:-3px;left:40px;">Điện thoại-Máy tính bảng</h4>
            </h4>
            <div class="row">
                <div class="col-lg-3 ads">
                    <img class="img-fluid baiphone" src="Libs/img-sanvchat01/3iphone-new.png">
                    <p class="text-center fa-2x text-light mt-1 mb-1">Mua Iphone</p>
                    <p class="text-center text-light mb-1"
                       style="font-weight: 600;font-size: 16px;margin-left:-5px;"> Tặng Gói bảo hiểm trị giá tới
                    </p>
                    <p class="text-center mb-1 " style="color:#ffea00;font-size: 40px;font-weight: 600;">17.990.000Đ
                    </p>
                    <p class="text-center text-light" style="font-weight: 600; margin-top:-10px;">Áp dụng cho các
                        model</p>
                    <div class="boxmenu">
                        <ul class="mt-1">

                            <li class="text-light"><a href="#"><?php echo $row_2['0']['menu_name']; ?></a></li>
                            <li class="text-light secondmenu1"><a href="#"><?php echo $row_2['1']['menu_name']; ?></a>
                            </li>
                        </ul>
                        <ul class="mt-1">
                            <li class="text-light"><a href="#"><?php echo $row_2['2']['menu_name']; ?></a></li>
                            <li class="text-light secondmenu2"><a href="#"><?php echo $row_2['3']['menu_name']; ?></a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-2 mainmenu" style="padding-right:10px;">
                    <ul>
                        <li>
                            <button id="defaultOpen1" class=" tablink1 "
                                    onclick="openPage1('first-col1', this, '#0b67c6')">Mua nhiều
                            </button>
                            <div id="first-col1" class="listmenu tabcontent1">
                                <?php
                                for ($i = 0; $i <= $row_num - 1; $i++) {
                                    if ($row[$i]['cat_parent'] == 10 && $row[$i]['cat_mua'] == 1) {
                                        $cat_name = $row[$i]['cat_name'];
                                        echo '<a href="#">' . $cat_name . '</a>';
                                    }
                                }
                                ?>
                            </div>
                        </li>
                        <li>
                            <button class=" tablink1 " onclick="openPage1('second-col1', this, '#0b67c6')">Xem nhiều
                            </button>
                            <div id="second-col1" class="listmenu tabcontent1">
                                <?php
                                for ($i = 0; $i <= $row_num - 1; $i++) {
                                    if ($row[$i]['cat_parent'] == 10 && $row[$i]['cat_xem'] == 1) {
                                        $cat_name = $row[$i]['cat_name'];
                                        echo '<a href="#">' . $cat_name . '</a>';
                                    }
                                }
                                ?>
                            </div>
                        </li>
                    </ul>

                </div>
                <div class="col-lg-7 card-deck clearfix">
                    <div class="grid-container">
                        <div class="grid-item item1">
                            <div class="text-item1">
                                <a href="#">
                                    <p style="color: #333;"><?php echo $row_3['0']['prd_name']; ?></br>Dung
                                        lượng <?php echo $row_3['0']['prd_rom']; ?></p>
                                </a>
                                <p>Từ <span><?php echo $row_3['0']['prd_price']; ?>đ</span></p>
                                <p>có <?php echo $row_3['0']['prd_status']; ?> nơi bán </p>
                                <p>
                                    <i class="fas fa-star" style="color:#879596"></i>
                                    <i class="fas fa-star" style="color:#879596"></i>
                                    <i class="fas fa-star" style="color:#879596"></i>
                                    <i class="fas fa-star" style="color:#879596"></i>
                                    <i class="fas fa-star" style="color:#879596"></i>
                                    (10)
                                </p>
                            </div>
                            <img class="img-fluid img-item1"
                                 src="Libs/img-sanvchat01/<?php echo $row_3['0']['prd_image']; ?>">
                        </div>
                        <div class="grid-item item2">
                            <img class="img-fluid img-item2"
                                 src="Libs/img-sanvchat01/<?php echo $row_3['1']['prd_image']; ?>">
                            <a href="#">
                                <p><?php echo $row_3['1']['prd_name']; ?> </br>dung
                                    lượng <?php echo $row_3['1']['prd_rom']; ?></p>
                            </a>
                            <p>Từ <span><?php echo $row_3['1']['prd_price']; ?></span> </br>
                                có <?php echo $row_3['1']['prd_status']; ?> nơi bán</p>


                        </div>
                        <div class="grid-item item2">
                            <img class="img-fluid img-item2"
                                 src="Libs/img-sanvchat01/<?php echo $row_3['2']['prd_image']; ?>">
                            <a href="#">
                                <p><?php echo $row_3['2']['prd_name']; ?> </br>dung
                                    lượng <?php echo $row_3['2']['prd_rom']; ?></p>
                            </a>
                            <p>Từ <span><?php echo $row_3['2']['prd_price']; ?></span> </br>
                                có <?php echo $row_3['2']['prd_status']; ?> nơi bán</p>


                        </div>
                        <div class="grid-item item2">
                            <img class="img-fluid img-item2"
                                 src="Libs/img-sanvchat01/<?php echo $row_3['3']['prd_image']; ?>">
                            <a href="#">
                                <p><?php echo $row_3['3']['prd_name']; ?> </br>dung
                                    lượng <?php echo $row_3['3']['prd_rom']; ?></p>
                            </a>
                            <p>Từ <span><?php echo $row_3['3']['prd_price']; ?></span> </br>
                                có <?php echo $row_3['3']['prd_status']; ?> nơi bán</p>


                        </div>
                        <div class="grid-item item2">
                            <img class="img-fluid img-item2"
                                 src="Libs/img-sanvchat01/<?php echo $row_3['4']['prd_image']; ?>">
                            <a href="#">
                                <p><?php echo $row_3['4']['prd_name']; ?> </br>dung
                                    lượng <?php echo $row_3['4']['prd_rom']; ?></p>
                            </a>
                            <p>Từ <span><?php echo $row_3['4']['prd_price']; ?></span> </br>
                                có <?php echo $row_3['4']['prd_status']; ?> nơi bán</p>


                        </div>
                        <div class="grid-item item2">
                            <img class="img-fluid img-item2"
                                 src="Libs/img-sanvchat01/<?php echo $row_3['5']['prd_image']; ?>">
                            <a href="#">
                                <p><?php echo $row_3['5']['prd_name']; ?> </br>dung
                                    lượng <?php echo $row_3['5']['prd_rom']; ?></p>
                            </a>
                            <p>Từ <span><?php echo $row_3['5']['prd_price']; ?></span> </br>
                                có <?php echo $row_3['5']['prd_status']; ?> nơi bán</p>


                        </div>
                        <div class="grid-item item2">
                            <img class="img-fluid img-item2"
                                 src="Libs/img-sanvchat01/<?php echo $row_3['6']['prd_image']; ?>">
                            <a href="#">
                                <p><?php echo $row_3['6']['prd_name']; ?> </br>dung
                                    lượng <?php echo $row_3['6']['prd_rom']; ?></p>
                            </a>
                            <p>Từ <span><?php echo $row_3['6']['prd_price']; ?></span> </br>
                                có <?php echo $row_3['6']['prd_status']; ?> nơi bán</p>


                        </div>


                    </div>
                </div>
            </div>
        </div>

        <div class="container content2" style="position:relative;">
            <div
                    style="width:26px; height:26px;background-color: #e2829d;padding:1px 9px;margin:25px 0 20px -15px;">
                <i class="fas fa-female"></i>
            </div>
            <h4 class="title-maincontent" style="position:absolute;top:-3px;left:40px;">Mẹ và bé</h4>
            <div class="row">
                <div class="col-lg-3 ads">
                    <img class="img-fluid baiphone" src="Libs/img-sanvchat01/bimsua-new.png">
                    <p class="text-center fa-2x text-light mt-1 mb-1">Mua Bỉm</p>
                    <p class="text-center text-light mb-1"
                       style="font-weight: 600;font-size: 16px;margin-left:-5px;"> Tặng phần quà trị giá tới
                    </p>
                    <p class="text-center mb-1 " style="color:#ffea00;font-size: 40px;font-weight: 600;">1.990.000Đ
                    </p>
                    <p class="text-center text-light" style="font-weight: 600; margin-top:-10px;">Áp dụng cho các
                        loại</p>
                    <div class="boxmenu">
                        <ul class="mt-1">
                            <li class="text-light"><a href="#"><?php echo $row_2['4']['menu_name']; ?></a></li>
                            <li class="text-light secondmenu1"><a href="#"><?php echo $row_2['5']['menu_name']; ?></a>
                            </li>
                        </ul>
                        <ul class="mt-1">
                            <li class="text-light"><a href="#"><?php echo $row_2['6']['menu_name']; ?></a></li>
                            <li class="text-light secondmenu2"><a href="#"><?php echo $row_2['7']['menu_name']; ?></a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-2 mainmenu">
                    <ul>
                        <li>
                            <button id="defaultOpen2" class="menuhover tablink2"
                                    onclick="openPage2('first-col2', this, '#c14a6c')">Mua nhiều
                            </button>
                            <div id="first-col2" class="listmenu tabcontent2">
                                <?php
                                for ($i = 0; $i <= $row_num - 1; $i++) {
                                    if ($row[$i]['cat_parent'] == 11 && $row[$i]['cat_mua'] == 1) {
                                        $cat_name = $row[$i]['cat_name'];
                                        echo '<a href="#">' . $cat_name . '</a>';
                                    }
                                }
                                ?>
                            </div>
                        </li>
                        <li>
                            <button class="menuhover tablink2" onclick="openPage2('second-col2', this, '#c14a6c')">Xem
                                nhiều
                            </button>
                            <div id="second-col2" class="listmenu tabcontent2">
                                <?php
                                for ($i = 0; $i <= $row_num - 1; $i++) {
                                    if ($row[$i]['cat_parent'] == 11 && $row[$i]['cat_xem'] == 1) {
                                        $cat_name = $row[$i]['cat_name'];
                                        echo '<a href="#">' . $cat_name . '</a>';
                                    }
                                }
                                ?>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-7 card-deck clearfix">
                    <div class="grid-container">
                        <div class="grid-item item1">
                            <div class="text-item1">
                                <a href="#">
                                    <p style="color: #333;"><?php echo $row_3['7']['prd_name']; ?></br> khối
                                        lượng <?php echo $row_3['7']['prd_rom']; ?></p>
                                </a>
                                <p>Từ <span><?php echo $row_3['7']['prd_price']; ?>đ</span></p>
                                <p>có <?php echo $row_3['7']['prd_status']; ?> nơi bán </p>
                                <p>
                                    <i class="fas fa-star" style="color:#879596"></i>
                                    <i class="fas fa-star" style="color:#879596"></i>
                                    <i class="fas fa-star" style="color:#879596"></i>
                                    <i class="fas fa-star" style="color:#879596"></i>
                                    <i class="fas fa-star" style="color:#879596"></i>
                                    (10)
                                </p>
                            </div>
                            <img class="img-fluid img-item1"
                                 src="Libs/img-sanvchat01/<?php echo $row_3['7']['prd_image']; ?>">
                        </div>
                        <div class="grid-item item2">
                            <img class="img-fluid img-item2"
                                 src="Libs/img-sanvchat01/<?php echo $row_3['8']['prd_image']; ?>">
                            <a href="#">
                                <p><?php echo $row_3['8']['prd_name']; ?></br>khối
                                    lượng <?php echo $row_3['8']['prd_rom']; ?></p>
                            </a>
                            <p>Từ <span><?php echo $row_3['8']['prd_price']; ?>đ</span> </br>
                                có <?php echo $row_3['8']['prd_status']; ?> nơi bán</p>


                        </div>
                        <div class="grid-item item2">
                            <img class="img-fluid img-item2"
                                 src="Libs/img-sanvchat01/<?php echo $row_3['9']['prd_image']; ?>">
                            <a href="#">
                                <p><?php echo $row_3['9']['prd_name']; ?></br>khối
                                    lượng <?php echo $row_3['9']['prd_rom']; ?></p>
                            </a>
                            <p>Từ <span><?php echo $row_3['9']['prd_price']; ?>đ</span> </br>
                                có <?php echo $row_3['9']['prd_status']; ?> nơi bán</p>


                        </div>
                        <div class="grid-item item2">
                            <img class="img-fluid img-item2"
                                 src="Libs/img-sanvchat01/<?php echo $row_3['10']['prd_image']; ?>">
                            <a href="#">
                                <p><?php echo $row_3['10']['prd_name']; ?></br>khối
                                    lượng <?php echo $row_3['10']['prd_rom']; ?></p>
                            </a>
                            <p>Từ <span><?php echo $row_3['10']['prd_price']; ?>đ</span> </br>
                                có <?php echo $row_3['10']['prd_status']; ?> nơi bán</p>


                        </div>
                        <div class="grid-item item2">
                            <img class="img-fluid img-item2"
                                 src="Libs/img-sanvchat01/<?php echo $row_3['11']['prd_image']; ?>">
                            <a href="#">
                                <p><?php echo $row_3['11']['prd_name']; ?></br>khối
                                    lượng <?php echo $row_3['11']['prd_rom']; ?></p>
                            </a>
                            <p>Từ <span><?php echo $row_3['11']['prd_price']; ?>đ</span> </br>
                                có <?php echo $row_3['11']['prd_status']; ?> nơi bán</p>


                        </div>
                        <div class="grid-item item2">
                            <img class="img-fluid img-item2"
                                 src="Libs/img-sanvchat01/<?php echo $row_3['12']['prd_image']; ?>">
                            <a href="#">
                                <p><?php echo $row_3['12']['prd_name']; ?></br>khối
                                    lượng <?php echo $row_3['12']['prd_rom']; ?></p>
                            </a>
                            <p>Từ <span><?php echo $row_3['12']['prd_price']; ?>đ</span> </br>
                                có <?php echo $row_3['12']['prd_status']; ?> nơi bán</p>


                        </div>
                        <div class="grid-item item2">
                            <img class="img-fluid img-item2"
                                 src="Libs/img-sanvchat01/<?php echo $row_3['13']['prd_image']; ?>">
                            <a href="#">
                                <p><?php echo $row_3['13']['prd_name']; ?></br>khối
                                    lượng <?php echo $row_3['13']['prd_rom']; ?></p>
                            </a>
                            <p>Từ <span><?php echo $row_3['13']['prd_price']; ?>đ</span> </br>
                                có <?php echo $row_3['13']['prd_status']; ?> nơi bán</p>


                        </div>


                    </div>
                </div>
            </div>
        </div>

        <div class="container content3" style="position:relative;">
            <div
                    style="width:26px; height:26px;background-color: #ff8400;padding:1px 4px;margin:25px 0 20px -15px;">
                <i class="fas fa-laptop"></i>
            </div>
            <h4 class="title-maincontent" style="position:absolute;top:-3px;left:40px;">Laptop</h4>
            <div class="row">
                <div class="col-lg-3 ads">
                    <img class="img-fluid baiphone" src="Libs/img-sanvchat01/laptop-new.png">
                    <p class="text-center fa-2x text-light mt-1 mb-1">Mua Laptop</p>
                    <p class="text-center text-light mb-1"
                       style="font-weight: 600;font-size: 16px;margin-left:-5px;"> Tặng Gói bảo hành trị giá tới
                    </p>
                    <p class="text-center mb-1 " style="color:#ffea00;font-size: 40px;font-weight: 600;">17.990.000Đ
                    </p>
                    <p class="text-center text-light" style="font-weight: 600; margin-top:-10px;">Áp dụng cho các
                        model</p>
                    <div class="boxmenu">
                        <ul class="mt-1">
                            <li class="text-light"><a href="#"><?php echo $row_2['8']['menu_name']; ?></a></li>
                            <li class="text-light secondmenu1"><a href="#"><?php echo $row_2['9']['menu_name']; ?></a>
                            </li>
                        </ul>
                        <ul class="mt-1">
                            <li class="text-light"><a href="#"><?php echo $row_2['10']['menu_name']; ?></a></li>
                            <li class="text-light secondmenu2"><a href="#"><?php echo $row_2['11']['menu_name']; ?></a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-2 mainmenu">
                    <ul>
                        <li>
                            <button id="defaultOpen3" class="menuhover tablink3"
                                    onclick="openPage3('first-col3', this, '#ea7b04')">Mua nhiều
                            </button>
                            <div id="first-col3" class="listmenu tabcontent3">
                                <?php
                                for ($i = 0; $i <= $row_num - 1; $i++) {
                                    if ($row[$i]['cat_parent'] == 12 && $row[$i]['cat_mua'] == 1) {
                                        $cat_name = $row[$i]['cat_name'];
                                        echo '<a href="#">' . $cat_name . '</a>';
                                    }
                                }
                                ?>
                            </div>
                        </li>
                        <li>
                            <button class="menuhover tablink3" onclick="openPage3('second-col3', this, '#ea7b04')">Xem
                                nhiều
                            </button>
                            <div id="second-col3" class="listmenu tabcontent3">
                                <?php
                                for ($i = 0; $i <= $row_num - 1; $i++) {
                                    if ($row[$i]['cat_parent'] == 12 && $row[$i]['cat_xem'] == 1) {
                                        $cat_name = $row[$i]['cat_name'];
                                        echo '<a href="#">' . $cat_name . '</a>';
                                    }
                                }
                                ?>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-7 card-deck clearfix">
                    <div class="grid-container">
                        <div class="grid-item item1">
                            <div class="text-item1">
                                <a href="#">
                                    <p style="color: #333;"><?php echo $row_3['14']['prd_name']; ?></br> dung
                                        lượng <?php echo $row_3['14']['prd_rom']; ?></p>
                                </a>
                                <p>Từ <span><?php echo $row_3['14']['prd_price']; ?>đ</span></p>
                                <p>có <?php echo $row_3['14']['prd_status']; ?> nơi bán </p>
                                <p>
                                    <i class="fas fa-star" style="color:#879596"></i>
                                    <i class="fas fa-star" style="color:#879596"></i>
                                    <i class="fas fa-star" style="color:#879596"></i>
                                    <i class="fas fa-star" style="color:#879596"></i>
                                    <i class="fas fa-star" style="color:#879596"></i>
                                    (10)
                                </p>
                            </div>
                            <img class="img-fluid img-item1"
                                 src="Libs/img-sanvchat01/<?php echo $row_3['14']['prd_image']; ?>">
                        </div>
                        <div class="grid-item item2">
                            <img class="img-fluid img-item2"
                                 src="Libs/img-sanvchat01/<?php echo $row_3['15']['prd_image']; ?>">
                            <a href="#">
                                <p><?php echo $row_3['15']['prd_name']; ?></br>dung
                                    lượng <?php echo $row_3['15']['prd_rom']; ?></p>
                            </a>
                            <p>Từ <span><?php echo $row_3['15']['prd_price']; ?>đ</span> </br>
                                có <?php echo $row_3['15']['prd_status']; ?> nơi bán</p>


                        </div>
                        <div class="grid-item item2">
                            <img class="img-fluid img-item2"
                                 src="Libs/img-sanvchat01/<?php echo $row_3['16']['prd_image']; ?>">
                            <a href="#">
                                <p><?php echo $row_3['16']['prd_name']; ?></br>dung
                                    lượng <?php echo $row_3['16']['prd_rom']; ?></p>
                            </a>
                            <p>Từ <span><?php echo $row_3['16']['prd_price']; ?>đ</span> </br>
                                có <?php echo $row_3['16']['prd_status']; ?> nơi bán</p>


                        </div>
                        <div class="grid-item item2">
                            <img class="img-fluid img-item2"
                                 src="Libs/img-sanvchat01/<?php echo $row_3['17']['prd_image']; ?>">
                            <a href="#">
                                <p><?php echo $row_3['17']['prd_name']; ?></br>dung
                                    lượng <?php echo $row_3['17']['prd_rom']; ?></p>
                            </a>
                            <p>Từ <span><?php echo $row_3['17']['prd_price']; ?>đ</span> </br>
                                có <?php echo $row_3['17']['prd_status']; ?> nơi bán</p>


                        </div>
                        <div class="grid-item item2">
                            <img class="img-fluid img-item2"
                                 src="Libs/img-sanvchat01/<?php echo $row_3['18']['prd_image']; ?>">
                            <a href="#">
                                <p><?php echo $row_3['18']['prd_name']; ?></br>dung
                                    lượng <?php echo $row_3['18']['prd_rom']; ?></p>
                            </a>
                            <p>Từ <span><?php echo $row_3['18']['prd_price']; ?>đ</span> </br>
                                có <?php echo $row_3['18']['prd_status']; ?> nơi bán</p>


                        </div>
                        <div class="grid-item item2">
                            <img class="img-fluid img-item2"
                                 src="Libs/img-sanvchat01/<?php echo $row_3['19']['prd_image']; ?>">
                            <a href="#">
                                <p><?php echo $row_3['19']['prd_name']; ?></br>dung
                                    lượng <?php echo $row_3['19']['prd_rom']; ?></p>
                            </a>
                            <p>Từ <span><?php echo $row_3['19']['prd_price']; ?>đ</span> </br>
                                có <?php echo $row_3['19']['prd_status']; ?> nơi bán</p>


                        </div>
                        <div class="grid-item item2">
                            <img class="img-fluid img-item2"
                                 src="Libs/img-sanvchat01/<?php echo $row_3['20']['prd_image']; ?>">
                            <a href="#">
                                <p><?php echo $row_3['20']['prd_name']; ?></br>dung
                                    lượng <?php echo $row_3['20']['prd_rom']; ?></p>
                            </a>
                            <p>Từ <span><?php echo $row_3['20']['prd_price']; ?>đ</span> </br>
                                có <?php echo $row_3['20']['prd_status']; ?> nơi bán</p>
                        </div>

                    </div>
                </div>
            </div>
            <div>
                <button class="text-maincontent mb-4" type="submit">Xem thêm sản phẩm</button>
            </div>
        </div>
    </div>

</div>
<div class="text-endmain">
    <button class="" type="button">Đăng bán ngay cho chúng tôi</button>
</div>


<!-- footer -->
<div id="footer">
    <div class="container ">
        <div class="row infor">
            <div class="col-lg-2">
                <ul>
                    <li>
                        <p class="title-footer">Về chúng tôi</p>
                    </li>
                    <li>
                        <p>Giới thiệu</p>
                    </li>
                    <li>
                        <p>Quy chế hoạt động</p>
                    </li>
                    <li>
                        <p>Câu hỏi thường gặp</p>
                    </li>
                </ul>
            </div>
            <div class="col-lg-2">
                <ul>
                    <li>
                        <p class="title-footer">Tài khoản</p>
                    </li>
                    <li>
                        <p>Đăng nhập</p>
                    </li>
                    <li>
                        <p>Đăng kí</p>
                    </li>
                    <li>
                        <p class="text-left">Chính sách bảo mật</p>
                    </li>
                    <li>
                        <p>và chia sẻ thông tin</p>
                    </li>
                </ul>
            </div>
            <div class="col-lg-3">
                <p class="title-footer">Theo dõi chúng tôi trên</p>

                <img class="img-fluid" src="Libs/img-sanvchat01/3icon-new.png">
            </div>
            <div class="col-lg-2">
                <p class="title-footer">Tải miễn phí app</h5>
                    <a href="#"><img class="img-fluid mt-3" src="Libs/img-sanvchat01/ggplay.png"></a>
                    <a href="#"><img class="img-fluid mt-4" src="Libs/img-sanvchat01/appstore.png"></a>
            </div>
            <div class="col-lg-3">
                <a href="#"><img class="mt-2" src="Libs/img-sanvchat01/fb.png"></a>
            </div>
        </div>
    </div>
</div>

<div id="endfooter" class="border-top">
    <div class="container">
        <p class="contact">LIÊN HỆ VỚI CHÚNG TÔI</p>
        <div class="row ">
            <div class="col-lg-6 address">
                <ul>
                    <li>
                        <p>Trụ sở chính:102 Thái Thịnh ,Phường Trung Liệt, Quận Đống Đa, Hà Nội</p>
                    </li>
                    <li>
                        <p><span>Hotline toàn quốc :(024) 12 234 456-0123 4565 678</span></p>
                    </li>
                </ul>
            </div>
            <div class="col-lg-6 address">
                <ul>
                    <li>
                        <p>Chi nhánh Đà Nẵng : Tầng 6,Số 53 Nguyễn Văn Linh,Quận Hải Châu,TP Đà Nẵng </p>
                    </li>
                    <li>
                        <p>Chi nhánh Hồ Chí Minh : Đường Lữ Gia,Phường 15, Quận 11,Hồ CHí Minh</p>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<script src="Libs/js/scrollbar.js"></script>
<script type="text/javascript">
    $('.owl-carousel').owlCarousel({
        rtl: true,
        loop: true,
        margin: 0,
        stagePadding: 26,
        dots: false,
        nav: true,
        scrollbarType: 'scroll',
        navText: ["<i class='fa fa-chevron-right fa-2x' aria-hidden='true'>",
            "<i class='fa fa-chevron-left fa-2x' aria-hidden='true'></i>"],
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 3
            },
            900: {
                items: 6
            },

        }

    });

</script>

<script type="text/javascript">
    function openPage1(pageName, elmnt, color) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent1");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablink1");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].style.backgroundColor = "";
        }
        document.getElementById(pageName).style.display = "block";
        elmnt.style.backgroundColor = color;
    }

    // Get the element with id="defaultOpen" and click on it
    document.getElementById("defaultOpen1").click();
</script>
<script type="text/javascript">
    function openPage2(pageName, elmnt, color) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent2");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablink2");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].style.backgroundColor = "";
        }
        document.getElementById(pageName).style.display = "block";
        elmnt.style.backgroundColor = color;
    }

    // Get the element with id="defaultOpen" and click on it
    document.getElementById("defaultOpen2").click();
</script>
<script type="text/javascript">
    function openPage3(pageName, elmnt, color) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent3");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablink3");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].style.backgroundColor = "";
        }
        document.getElementById(pageName).style.display = "block";
        elmnt.style.backgroundColor = color;
    }

    // Get the element with id="defaultOpen" and click on it
    document.getElementById("defaultOpen3").click();
</script>
</body>


</html>