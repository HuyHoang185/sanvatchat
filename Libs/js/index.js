$('.owl-carousel').owlCarousel({
    rtl:true,
    loop:true,
    margin:10,
    nav:false,
    dot:false,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:3
        },
        900:{
            items:5
        },
       
    }
   
})

